import os
import time
import argparse
from collections import namedtuple
from multiprocessing import Pool

import dlib
import cv2


Point = namedtuple('Point', 'x y')
Rect = namedtuple('Rect', 'left top right bottom')

DLIB_COLOR_G = dlib.rgb_pixel(0, 255, 0)
DLIB_COLOR_R = dlib.rgb_pixel(255, 0, 0)

face_detector = dlib.get_frontal_face_detector()


def main(filepath, upsample=1, preview=False, save=False):
    image = cv2.imread(filepath)

    if image is None:
        return

    height, width, _ = image.shape

    t1 = time.time()
    faces = face_detector(image, upsample)
    t2 = time.time()

    detection_time = t2-t1

    print "[{:.2f}s] found {} faces - {}".format(detection_time, len(faces), filepath)

    if not faces:
        return

    # punto central de todos los centros de rostros
    face_centers = [Point(face.center().x, face.center().y) for face in faces]
    centroid = calc_centroid(face_centers)

    # maximo cuadrado posible y centrado
    square = calc_max_square_centered(centroid, width, height)

    if preview:
        win = dlib.image_window()
        image_rgb = cv2.cvtColor(image, cv2.cv.CV_BGR2RGB)
        win.set_image(image_rgb)

        sq = dlib.rectangle(square.left, square.top, square.right, square.bottom)
        win.add_overlay(sq, DLIB_COLOR_R)

        for face in faces:
            win.add_overlay(face, DLIB_COLOR_G)

        dlib.hit_enter_to_continue()
        win.clear_overlay()

    if save:
        cropped_image = image[square.top:square.bottom, square.left:square.right]
        dest = os.path.join(os.path.dirname(filepath), "crop_%s" % os.path.basename(filepath))
        cv2.imwrite(dest, cropped_image)


def calc_centroid(points):
    length = len(points)

    if length == 1:
        return Point(points[0].x, points[0].y)

    sumX = sum([p.x for p in points])
    sumY = sum([p.y for p in points])

    centroidX = sumX / length
    centroidY = sumY / length

    return Point(centroidX, centroidY)


def calc_max_square_centered(C, width, height):
    """
         square_side   square_dx
          _ _ _ _ _ _|_ _ _ _
         |     .     |       |
         |     .     |       |
       - | - - - - - - - - - |
         |     .  C  |       |
         |_ _ _._ _ _|_ _ _ _|
               .  .
               .  .
               -dx-
               .
               .
        (square_side/2)

    """
    square_side = min(width, height)
    square_dx =  width - square_side
    square_dy = height - square_side

    dx = C.x - square_side/2
    dy = C.y - square_side/2

    if dx > 0:
        dx = min(dx, square_dx)

    if dy > 0:
        dy = min(dy, square_dy)

    left = max(0, dx)
    top = max(0, dy)
    right = left + square_side
    bottom = top + square_side

    return Rect(left, top, right, bottom)


def start_worker(args):
    return main(*args)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('images', nargs='+')
    parser.add_argument('-s', '--save', action='store_true', help="Save square crop images")
    parser.add_argument('-p', '--preview', action='store_true', help="Hit enter to continue")
    parser.add_argument('-w', '--workers', type=int, help="Workers in multiprocessing")
    parser.add_argument('--upsample', type=int, help="dlib's face detector upsample", default=1)
    args = parser.parse_args()

    if args.workers and not args.preview:
        pool = Pool(args.workers)
        pool.map(start_worker, [(filename, args.upsample, False, args.save) for filename in args.images])
    else:
        for filepath in args.images:
            main(filepath, args.upsample, args.preview, args.save)
