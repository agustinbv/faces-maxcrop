# Faces detection & max square crop

Install dlib dependencies
`$ sudo apt-get install python-opencv python-dev libboost-python-dev`

As documented on dlib, for more performance install OpenBLAS
`$ sudo apt-get install libopenblas-dev liblapack-dev`

```
$ git clone https://github.com/davisking/dlib
$ cd dlib
$ python setup.py install --yes USE_AVX_INSTRUCTIONS
```

## Run
```
# preview without saving
$ python crop.py path/to/images/* --preview

# preview and save (images are saved as "crop_{filename}")
$ python crop.py path/to/images/* -p -s

# run 4 process in parallel
$ python crop.py path/to/images/* -s -w 4

```

![Captura de pantalla_2016-08-19_14-16-14.png](https://bitbucket.org/repo/pL4b7A/images/610263512-Captura%20de%20pantalla_2016-08-19_14-16-14.png)

## Help

```
$ python crop.py --help
usage: crop.py [-h] [-s] [-p] [-w WORKERS] [--upsample UPSAMPLE]
               images [images ...]

positional arguments:
  images

optional arguments:
  -h, --help            show this help message and exit
  -s, --save            Save square crop images
  -p, --preview         Hit enter to continue
  -w WORKERS, --workers WORKERS
                        Workers in multiprocessing
  --upsample UPSAMPLE   dlib's face detector upsample
```

## REFERENCE
[https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78](https://medium.com/@ageitgey/machine-learning-is-fun-part-4-modern-face-recognition-with-deep-learning-c3cffc121d78)